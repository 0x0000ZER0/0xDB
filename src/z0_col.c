#include "z0_defs.h"

#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

uint32_t
z0_col_add(z0_db *db, uint32_t t_id, char *name, uint8_t name_len)
{
	z0_tbl *tbl;
	tbl = &db->tbls[t_id];

	if (tbl->c_len == tbl->c_cap) {
		tbl->c_cap *= 2;

		z0_col *cols;
		cols = realloc(tbl->cols, tbl->c_cap);
		if (cols == NULL) {
#ifdef Z0_DEBUG
			fprintf(stderr, "ERR: could not allocate enough memory for the columns.\n");
#endif
			return tbl->c_cap;
		}

		tbl->cols = cols;
	}

	uint32_t uid;
	uid = tbl->c_len;

	z0_col *col;
	col = &tbl->cols[uid];
	
	memcpy(col->name, name, name_len);
	col->n_len = name_len;

	++tbl->c_len;
	return uid;	
}
