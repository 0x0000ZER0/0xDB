#include "z0_defs.h"

#include <stdio.h>

void
z0_db_hex(void *data, size_t size)
{
	uint8_t *ptr;
	ptr = data;

	for (size_t i = 0; i < size; ++i) {
		printf("%02X", *(ptr + i));
		if ((i + 1) % 16 == 0)
			printf("\n");
		else if ((i + 1) % 8 == 0)
			printf(" ");
	}
}
