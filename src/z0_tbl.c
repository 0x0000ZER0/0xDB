#include "z0_defs.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h> 

uint32_t
z0_tbl_add(z0_db *db, char *name, uint8_t name_len, size_t r_size)
{
	if (db->t_len == db->t_cap) {
		db->t_cap *= 2;
		
		z0_tbl *tbls;
		tbls = realloc(db->tbls, sizeof (z0_tbl) * db->t_cap);
		if (tbls == NULL) {
#ifdef Z0_DEBUG
			fprintf(stderr, "ERR: could not reallocate enough memory for tbls.\n");
#endif
			return db->t_cap;
		}

		db->tbls = tbls;
	}

	uint32_t uid;
	uid = db->t_len;

	z0_tbl *tbl;
	tbl = &db->tbls[uid];

	memcpy(tbl->name, name, name_len);
	tbl->n_len  = name_len;
	tbl->c_cap  = 4;
	tbl->c_len  = 0;
	tbl->r_len  = 0;
	tbl->r_size = r_size;
	tbl->cols   = malloc(sizeof (z0_col) * tbl->c_cap);
	if (tbl->cols == NULL) {
#ifdef Z0_DEBUG
			fprintf(stderr, "ERR: could not allocate enough memory for tbl columns.\n");
#endif
		return db->t_cap;
	}

	++db->t_len;

	return uid;
}
