#include "z0_defs.h"

#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

uint32_t
z0_row_add(z0_db *db, uint32_t t_id, void *data)
{
	z0_tbl *tbl;
	tbl = &db->tbls[t_id];

	size_t size;
	size = tbl->r_size;

	uint32_t uid;
	uid = tbl->r_len;

	++tbl->r_len;
	
	void *rows;
	if (uid > 0) {
		rows = realloc(tbl->rows, size * tbl->r_len);
	} else {
		rows = malloc(size * tbl->r_len);
	}

	if (rows == NULL) {
#ifdef Z0_DEBUG
		fprintf(stderr, "ERR: could not allocate enough memory for the row.\n");
#endif
		return tbl->r_len;
	}

	tbl->rows = rows;

	uint8_t *ptr;
	ptr = tbl->rows;
	ptr = ptr + uid * size;
	memcpy(ptr, data, size);

	return uid;	
}

void*
z0_row_get(z0_db *db, uint32_t t_id, uint32_t r_id)
{
	z0_tbl *tbl;
	tbl = &db->tbls[t_id];

	uint8_t *ptr;
	ptr  = tbl->rows;
	ptr += tbl->r_size * r_id;

	return (void*)ptr;
}
