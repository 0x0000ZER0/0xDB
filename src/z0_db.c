#include "z0_defs.h"

#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void
z0_db_init(z0_db *db)
{
	db->t_cap = 4;
	db->t_len = 0;

	db->tbls = malloc(sizeof (z0_tbl) * db->t_cap);
	if (db->tbls == NULL) {
#ifdef Z0_DEBUG
		fprintf(stderr, "ERR: could not allocate enough memory for the database.\n");
#endif
	}
}

void
z0_db_save(z0_db *db, const char *path)
{
	/*
	FILE FORMAT FOR THE DATABASE
	----------------------------

	u32  tables_length
		u8   table0_name_length
		u8[] table0_name
		u32  table0_columns_length
			u8   table0_column0_name_length
			u8[] table0_column0_name
			u8   table0_column1_name_length
			u8[] table0_column1_name
			...
			u8   table0_columnN_name_length
			u8[] table0_columnN_name
		u64  table0_row_size
		u32  table0_rows_length
			u8[] table0_row0
			u8[] table0_row1
			...
			u8[] table0_rowN
		----------------------------------------
		u8   table1_name_length
		u8[] table1_name
		u32  table1_columns_length
			u8   table1_column0_name_length
			u8[] table1_column0_name
			u8   table1_column1_name_length
			u8[] table1_column1_name
			...
			u8   table1_columnN_name_length
			u8[] table1_columnN_name
		u64  table1_row_size
		u32  table1_rows_length
			u8[] table1_row0
			u8[] table1_row1
			...
			u8[] table1_rowN
		----------------------------------------
		...
	*/

	int file;
	file = open(path, O_CREAT | O_RDWR | O_APPEND, S_IRUSR | S_IWUSR);
	if (file == -1) {
		fprintf(stderr, "ERR: could not open the file.\n");
		return;
	}
	
	ssize_t bytes;
	bytes = write(file, &db->t_len, sizeof (db->t_len));
	printf("INFO: wrote %zd bytes.\n", bytes);
	for (uint32_t t = 0; t < db->t_len; ++t) {
		z0_tbl *tbl;
		tbl = &db->tbls[t];

		bytes = write(file, &tbl->n_len, sizeof (tbl->n_len));
		printf("INFO: wrote %zd bytes.\n", bytes);
		bytes = write(file, tbl->name, tbl->n_len);
		printf("INFO: wrote %zd bytes.\n", bytes);
		bytes = write(file, &tbl->c_len, sizeof (tbl->c_len));
		printf("INFO: wrote %zd bytes.\n", bytes);
		for (uint32_t c = 0; c < tbl->c_len; ++c) {
			z0_col *col;
			col = &tbl->cols[c];	

			bytes = write(file, &col->n_len, sizeof (col->n_len));
			printf("INFO: wrote %zd bytes.\n", bytes);
			bytes = write(file, col->name, col->n_len);
			printf("INFO: wrote %zd bytes.\n", bytes);
		}

		bytes = write(file, &tbl->r_size, sizeof (tbl->r_size));
		printf("INFO: wrote %zd bytes.\n", bytes);
		bytes = write(file, &tbl->r_len, sizeof (tbl->r_len));
		printf("INFO: wrote %zd bytes.\n", bytes);

		uint8_t *ptr;
		ptr = tbl->rows;
		for (uint32_t r = 0; r < tbl->r_len; ++r) {
			bytes = write(file, ptr + r * tbl->r_size, tbl->r_size);
			printf("INFO: wrote %zd bytes.\n", bytes);
		}
	}

	close(file);
}

void
z0_db_load(z0_db *db, const char *path)
{
	int file;
	file = open(path, O_RDONLY);
	if (file == -1) {
		fprintf(stderr, "ERR: could not open the file.\n");
		return;
	}

	ssize_t bytes;
	bytes = read(file, &db->t_len, sizeof (db->t_len));
	printf("INFO: read %zd bytes.\n", bytes);

	db->t_cap   = db->t_len;
	db->t_cap  |= db->t_cap >> 1;
	db->t_cap  |= db->t_cap >> 2;
	db->t_cap  |= db->t_cap >> 4;
	db->t_cap  |= db->t_cap >> 8;
	db->t_cap  |= db->t_cap >> 16;
	db->t_cap   = db->t_cap + 1;
	db->tbls    = malloc(sizeof (z0_tbl) * db->t_cap);

	for (uint32_t t = 0; t < db->t_len; ++t) {
		z0_tbl *tbl;
		tbl = &db->tbls[t];

		bytes = read(file, &tbl->n_len, sizeof (tbl->n_len));
		printf("INFO: read %zd bytes.\n", bytes);
		bytes = read(file, tbl->name, tbl->n_len);
		printf("INFO: read %zd bytes.\n", bytes);
		bytes = read(file, &tbl->c_len, sizeof (tbl->c_len));
		printf("INFO: read %zd bytes.\n", bytes);

		tbl->c_cap  = tbl->c_len;
		tbl->c_cap |= tbl->c_cap >> 1;	
		tbl->c_cap |= tbl->c_cap >> 2;	
		tbl->c_cap |= tbl->c_cap >> 4;	
		tbl->c_cap |= tbl->c_cap >> 8;	
		tbl->c_cap |= tbl->c_cap >> 16;	
		tbl->c_cap  = tbl->c_cap + 1;
		tbl->cols   = malloc(sizeof (z0_col) * tbl->c_cap);

		for (uint32_t c = 0; c < tbl->c_len; ++c) {
			z0_col *col;
			col = &tbl->cols[c];

			bytes = read(file, &col->n_len, sizeof (col->n_len));
			printf("INFO: read %zd bytes.\n", bytes);
			bytes = read(file, col->name, col->n_len);
			printf("INFO: read %zd bytes.\n", bytes);
		}

		bytes = read(file, &tbl->r_size, sizeof (tbl->r_size));
		printf("INFO: read %zd bytes.\n", bytes);
		bytes = read(file, &tbl->r_len, sizeof (tbl->r_len));
		printf("INFO: read %zd bytes.\n", bytes);

		tbl->rows = malloc(tbl->r_size * tbl->r_len);

		uint8_t *ptr;
		ptr = tbl->rows;
		for (uint32_t r = 0; r < tbl->r_len; ++r) {
			bytes = read(file, ptr + r * tbl->r_size, tbl->r_size);
			printf("INFO: read %zd bytes.\n", bytes);
		}
	}
	close(file);
}

void
z0_db_dbg(z0_db *db)
{
	z0_tbl *tbl;
	for (uint32_t i = 0; i < db->t_len; ++i) {
		tbl = &db->tbls[i];
		
		printf("=============== %.*s [%u] ================\n", (int)tbl->n_len, tbl->name, i);
		for (uint32_t c = 0; c < tbl->c_len; ++c)
			printf("%.*s ", (int)tbl->cols[c].n_len, tbl->cols[c].name);
		printf("\n----------------------\n");
		
		size_t off;
		off = 0;
		for (uint32_t r = 0; r < tbl->r_len; ++r) {
			printf("uid: %u\n", r);

			uint8_t *rows;
			rows = tbl->rows;
			
			uint8_t *row;
			row = rows + off;
			z0_db_hex(row, tbl->r_size);

			off += tbl->r_size;

			if (r != tbl->r_len - 1)
				printf("\n----------------------\n");
			else
				printf("\n");
		}
	}	
}

void
z0_db_free(z0_db *db)
{
	for (uint32_t i = 0; i < db->t_len; ++i) {
		free(db->tbls[i].cols);
		if (db->tbls[i].r_len > 0)
			free(db->tbls[i].rows);
	}
	free(db->tbls);
}

