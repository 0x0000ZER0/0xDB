#include "z0_defs.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define RESET   "\033[0m"
#define RED     "\033[31m" 
#define GREEN   "\033[32m"

#define Z0_TEST_NAME(name) printf("========== %s ==========\n", name)
#define Z0_TEST_DESC(text) printf("DESC:   %s\n", text)
#define Z0_TEST_ASSERT(condition) \
	if ((condition)) { \
		printf("RESULT: " GREEN "PASSED" RESET "\n"); \
	} else { \
		printf("RESULT: " RED "FAILED" RESET "\n"); \
	}


static void
z0_test_tbl_add()
{
	Z0_TEST_NAME("TEST TABLE CREATION");
	z0_db db;
	z0_db_init(&db);
	
	char    name_0[]   = "table 0";
	uint8_t name_len_0 = sizeof (name_0) - 1;

	uint32_t tbl_0;
	tbl_0 = z0_tbl_add(&db, name_0, name_len_0, 0);
	(void)tbl_0;

	char    name_1[]   = "table 1";
	uint8_t name_len_1 = sizeof (name_1) - 1;

	uint32_t tbl_1;
	tbl_1 = z0_tbl_add(&db, name_1, name_len_1, 0);
	(void)tbl_1;
	
	char    name_2[]   = "table 2";
	uint8_t name_len_2 = sizeof (name_2) - 1;

	uint32_t tbl_2;
	tbl_2 = z0_tbl_add(&db, name_2, name_len_2, 0);
	(void)tbl_2;

	Z0_TEST_ASSERT(db.t_len == 3);
	Z0_TEST_ASSERT(strncmp(name_0, (char*)db.tbls[0].name, name_len_0) == 0);
	Z0_TEST_ASSERT(db.tbls[0].n_len == name_len_0);
	Z0_TEST_ASSERT(strncmp(name_1, (char*)db.tbls[1].name, name_len_1) == 0);
	Z0_TEST_ASSERT(db.tbls[1].n_len == name_len_1);
	Z0_TEST_ASSERT(strncmp(name_2, (char*)db.tbls[2].name, name_len_2) == 0);
	Z0_TEST_ASSERT(db.tbls[2].n_len == name_len_2);

	z0_db_free(&db);
}

static void
z0_test_col_add() 
{
	Z0_TEST_NAME("TEST COLUMN CREATION");

	z0_db db;
	z0_db_init(&db);
	
	char    tbl_name[]   = "table";
	uint8_t tbl_name_len = sizeof (tbl_name) - 1;

	uint32_t tbl_uid;
	tbl_uid = z0_tbl_add(&db, tbl_name, tbl_name_len, 0);

	char    col_name_0[]   = "column 0";
	uint8_t col_name_len_0 = sizeof (col_name_0) - 1;

	uint32_t col_0;
	col_0 = z0_col_add(&db, tbl_uid, col_name_0, col_name_len_0);
	(void)col_0;

	char    col_name_1[]   = "column 1";
	uint8_t col_name_len_1 = sizeof (col_name_1) - 1;

	uint32_t col_1;
	col_1 = z0_col_add(&db, tbl_uid, col_name_1, col_name_len_1);
	(void)col_1;

	char    col_name_2[]   = "column 2";
	uint8_t col_name_len_2 = sizeof (col_name_2) - 1;

	uint32_t col_2;
	col_2 = z0_col_add(&db, tbl_uid, col_name_2, col_name_len_2);
	(void)col_2;

	z0_tbl *tbl;
	tbl = &db.tbls[tbl_uid];

	Z0_TEST_ASSERT(tbl->c_len == 3);
	Z0_TEST_ASSERT(strncmp((char*)tbl->cols[col_0].name, col_name_0, col_name_len_0) == 0);
	Z0_TEST_ASSERT(tbl->cols[col_0].n_len == col_name_len_0);
	Z0_TEST_ASSERT(strncmp((char*)tbl->cols[col_1].name, col_name_1, col_name_len_1) == 0);
	Z0_TEST_ASSERT(tbl->cols[col_1].n_len == col_name_len_1);
	Z0_TEST_ASSERT(strncmp((char*)tbl->cols[col_2].name, col_name_2, col_name_len_2) == 0);
	Z0_TEST_ASSERT(tbl->cols[col_2].n_len == col_name_len_2);

	z0_db_free(&db);
}

static void
z0_test_row_add()
{
	Z0_TEST_NAME("TEST ROW CREATION");

	z0_db db;
	z0_db_init(&db);
	
	char    tbl_name[]   = "table";
	uint8_t tbl_name_len = sizeof (tbl_name) - 1;
	typedef struct {
		uint64_t x0;
		uint8_t  x1;
	} row_data; 

	uint32_t tbl_uid;
	tbl_uid = z0_tbl_add(&db, tbl_name, tbl_name_len, sizeof (row_data));

	char    col_name_0[]   = "column 0";
	uint8_t col_name_len_0 = sizeof (col_name_0) - 1;

	uint32_t col_0;
	col_0 = z0_col_add(&db, tbl_uid, col_name_0, col_name_len_0);
	(void)col_0;

	char    col_name_1[]   = "column 1";
	uint8_t col_name_len_1 = sizeof (col_name_1) - 1;

	uint32_t col_1;
	col_1 = z0_col_add(&db, tbl_uid, col_name_1, col_name_len_1);
	(void)col_1;

	row_data data_0;
	data_0.x0 = UINT64_MAX;
	data_0.x1 = UINT8_MAX;	

	uint32_t row_0;	
	row_0 = z0_row_add(&db, tbl_uid, &data_0);

	row_data data_1;
	data_1.x0 = 0xFFEEDDCCBBAA9988;
	data_1.x1 = 0x1F;

	uint32_t row_1;	
	row_1 = z0_row_add(&db, tbl_uid, &data_1);

	z0_tbl *tbl;
	tbl = &db.tbls[tbl_uid];

	Z0_TEST_ASSERT(tbl->r_len == 2);
	
	row_data *get_0;
	get_0 = z0_row_get(&db, tbl_uid, row_0);
	Z0_TEST_ASSERT(data_0.x0 = get_0->x0);
	Z0_TEST_ASSERT(data_0.x1 = get_0->x1);

	row_data *get_1;
	get_1 = z0_row_get(&db, tbl_uid, row_1);
	Z0_TEST_ASSERT(data_1.x0 = get_1->x0);
	Z0_TEST_ASSERT(data_1.x1 = get_1->x1);

	z0_db_free(&db);
}

static void
z0_test_save_only()
{
	Z0_TEST_NAME("TEST DB SAVE ONLY");

	z0_db db;
	z0_db_init(&db);
	
	char    tbl_name[]   = "table";
	uint8_t tbl_name_len = sizeof (tbl_name) - 1;
	typedef struct {
		uint64_t x0;
		uint8_t  x1;
	} row_data; 

	uint32_t tbl_uid;
	tbl_uid = z0_tbl_add(&db, tbl_name, tbl_name_len, sizeof (row_data));

	char    col_name_0[]   = "column 0";
	uint8_t col_name_len_0 = sizeof (col_name_0) - 1;

	uint32_t col_0;
	col_0 = z0_col_add(&db, tbl_uid, col_name_0, col_name_len_0);
	(void)col_0;

	char    col_name_1[]   = "column 1";
	uint8_t col_name_len_1 = sizeof (col_name_1) - 1;

	uint32_t col_1;
	col_1 = z0_col_add(&db, tbl_uid, col_name_1, col_name_len_1);
	(void)col_1;

	row_data data_0;
	data_0.x0 = UINT64_MAX;
	data_0.x1 = UINT8_MAX;	

	uint32_t row_0;	
	row_0 = z0_row_add(&db, tbl_uid, &data_0);
	(void)row_0;

	row_data data_1;
	data_1.x0 = 0xFFEEDDCCBBAA9988;
	data_1.x1 = 0x1F;

	uint32_t row_1;	
	row_1 = z0_row_add(&db, tbl_uid, &data_1);
	(void)row_1;

	const char *path = "./db.meta";
	z0_db_save(&db, path);

	Z0_TEST_ASSERT(access(path, R_OK) == 0);

	unlink(path);

	z0_db_free(&db);
}

static void
z0_test_save_load()
{
	Z0_TEST_NAME("TEST DB SAVE AND LOAD");

	z0_db db;
	z0_db_init(&db);
	
	char    tbl_name[]   = "table";
	uint8_t tbl_name_len = sizeof (tbl_name) - 1;
	typedef struct {
		uint64_t x0;
		uint8_t  x1;
	} row_data; 

	uint32_t tbl_uid;
	tbl_uid = z0_tbl_add(&db, tbl_name, tbl_name_len, sizeof (row_data));

	char    col_name_0[]   = "column 0";
	uint8_t col_name_len_0 = sizeof (col_name_0) - 1;

	uint32_t col_0;
	col_0 = z0_col_add(&db, tbl_uid, col_name_0, col_name_len_0);
	(void)col_0;

	char    col_name_1[]   = "column 1";
	uint8_t col_name_len_1 = sizeof (col_name_1) - 1;

	uint32_t col_1;
	col_1 = z0_col_add(&db, tbl_uid, col_name_1, col_name_len_1);
	(void)col_1;

	row_data data_0;
	data_0.x0 = UINT64_MAX;
	data_0.x1 = UINT8_MAX;	

	uint32_t row_0;	
	row_0 = z0_row_add(&db, tbl_uid, &data_0);
	(void)row_0;

	row_data data_1;
	data_1.x0 = 0xFFEEDDCCBBAA9988;
	data_1.x1 = 0x1F;

	uint32_t row_1;	
	row_1 = z0_row_add(&db, tbl_uid, &data_1);
	(void)row_1;

	const char *path = "./db.meta";
	z0_db_save(&db, path);

	z0_db db2;
	z0_db_load(&db2, path);

	unlink(path);

	Z0_TEST_ASSERT(db.t_len == db2.t_len);

	z0_db_free(&db2);
	z0_db_free(&db);
}

int
main(void)
{
	z0_test_tbl_add();
	z0_test_col_add();
	z0_test_row_add();
	z0_test_save_only();
	z0_test_save_load();
	return 0;
}
