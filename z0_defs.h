#ifndef Z0_DEFS
#define Z0_DEFS

#include <stdint.h> 
#include <stddef.h> 

typedef struct {
	uint8_t name[UINT8_MAX];
	uint8_t n_len;
} z0_col;

typedef struct {
	uint8_t name[UINT8_MAX];
	uint8_t n_len;	

	z0_col   *cols;
	uint32_t  c_len;
	uint32_t  c_cap;

	void	 *rows;
	uint64_t    r_size;
	uint32_t  r_len;
} z0_tbl;

typedef struct {
	z0_tbl   *tbls;	
	uint32_t  t_len;
	uint32_t  t_cap;
} z0_db;

uint32_t
z0_tbl_add(z0_db*, char*, uint8_t, size_t);

uint32_t
z0_col_add(z0_db*, uint32_t, char*, uint8_t);

uint32_t
z0_row_add(z0_db*, uint32_t, void*);

void*
z0_row_get(z0_db*, uint32_t, uint32_t);

void
z0_db_init(z0_db*);

void
z0_db_save(z0_db*, const char*);

void
z0_db_load(z0_db*, const char*);

void
z0_db_hex(void*, size_t);

void
z0_db_dbg(z0_db*);

void
z0_db_free(z0_db*);

#endif
