SRC=$(wildcard src/*c)
OUT=db.out

.PHONY: test
test:
	gcc -g ./test/z0_test.c $(SRC) -I. -Wall -Wextra -fsanitize=leak,address,undefined -o $(OUT) -DZ0_DEBUG \
	&& ./$(OUT) \
	&& rm ./$(OUT)

.PHONY: clear
clear:
	rm $(OUT)
